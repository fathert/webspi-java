![LogoColorTextBelow.jpeg](https://bitbucket.org/repo/5pxrEo/images/2313255042-LogoColorTextBelow.jpeg)

# README #

## What is WebSP? ##
**WebSP** is a slim, open-source framework that exposes stored procedures on a database server as web-services which can be called directly or used to form the basis of web or mobile applications. It was written primarily with the IBM i (also known as AS/400, iSeries, system i) in mind but is not specifically tied to any database engine. 

The framework comprises a JavaEE component that runs under [TomEE](https://tomee.apache.org/) (Tomcat with JavaEE extensions) and backend functions written in the language of the database server being accessed. This repository hosts the Java layer, a platform specific backend is also required, currently only an IBM i backend is provided and can be found [here](https://bitbucket.org/fathert/webspi-ibmi) but others can easily created by implementing a [procedure resolution procedure](https://bitbucket.org/fathert/webspi-java/wiki/Procedure%20Resolution) on the required platform.

## How do I get set up? ##
The simplest way is to download the latest WAR file from the [downloads](https://bitbucket.org/fathert/webspi-java/downloads/) section and deploy it to your TomEE server.

### Prerequisites ###

* JDK 8 installed
* Apache TomEE version 7.0.0 - 7.0.4 (see installation instructions below) **NOTE** WebSPi does not currently work with version 7.05 and above.

### Installing TomEE ###
See Wiki page [here](https://bitbucket.org/fathert/webspi-java/wiki/Installing%20TomEE%20on%20IBM%20i).

### Building WebSP ###
The project can be build from source using NetBeans 8.1 or later.

### Deploying WebSP ###
The WAR file can be deployed to TomEE using the Tomcat interface by signing into the manager app with the credentials tomee/[password] (as set-up in your `./conf/tomee-users.xml` file). 

Alternatively, the WAR file can be copied into the TomEE installation folder, from where it will be automatically deployed.

If the WAR file has been deployed correctly then visiting the URL `http://[YOUR_HOST]:[PORT]/webspi` should display the WebSPi welcome page. **NOTE: Until a suitable backend has been installed WebSPi will not yet work!**

### Database Backend ###

The backend components at a minimum must comprise a stored procedure resolver procedure. An IBM i backend implementation can be found [here](https://bitbucket.org/fathert/webspi-ibmi/src/master/).

### Contribution guidelines ###
TBC.

### Who do I talk to? ###

* Tim Fathers (tim@fathers.me.uk)