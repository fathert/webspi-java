/* 
 * Copyright (C) 2015-2017 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.webspi;

import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/*
 * Default Datasource Resolver
 * Finds a TomEE datasource in the tomee.xml file.
 */
@Stateless
public class DefaultTomeeDatasourceResolver implements DatasourceResolver {
    private static final Logger LOGGER = Logger.getLogger(DefaultTomeeDatasourceResolver.class.getName());
    private @Resource SessionContext sessionContext;

    @Override
    public DataSource resolve(Map context) throws Exception {

        DataSource ds = null;
        String datasourceName = context.get("datasource").toString();
        try {
            ds = (DataSource) sessionContext.lookup("java:openejb/Resource/" + datasourceName);
        }
        catch (Exception ex) {
            String message = "Datasource not found: " + datasourceName;
            throw new Exception(message);
        }
        
        return ds;
    }
}
