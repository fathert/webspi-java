/* 
 * Copyright (C) 2015-2016 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.webspi;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class SQLExceptionMapper implements ExceptionMapper<SQLException> {
    private static final Logger LOGGER = Logger.getLogger(SQLExceptionMapper.class.getName());

    /**
     * SQLExceptionMapper
     * @param exception
     * @return
     * Maps an SQL exception to an HTTP status code. Any SQL status starting 38xxx
     * will be mapped to an HTTP status matching the final three digits of the status
     * e..g 38401 = HTTP 401. Any unrecognised status is mapped to 500. The HTTP
     * headers "x-exception-message", "x-sql-state", "x-error-code" are also set
     * accordingly.
     */
    @Override
    public Response toResponse(SQLException exception) {
        Response.Status httpStatus;
        
        LOGGER.log(Level.SEVERE, "SQL exception: ", exception);
        
        String state = exception.getSQLState();
        if (state.startsWith("38")) {
            String response = state.substring(2);
            
            switch (response) {
                case "400":
                    httpStatus = Response.Status.BAD_REQUEST;
                    break;
                case "401":
                    httpStatus = Response.Status.UNAUTHORIZED;
                    break;
                case "403":
                    httpStatus = Response.Status.FORBIDDEN;
                    break;
                case "404":
                    httpStatus = Response.Status.NOT_FOUND;
                    break;
                case "409":
                    httpStatus = Response.Status.CONFLICT;
                    break;
                case "412":
                    httpStatus = Response.Status.PRECONDITION_FAILED;
                    break;
                default:
                    httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
                    break;
            }
        }
        else {
            httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
        }
        
        return Response.status(httpStatus).
                header("x-exception-message", exception.getMessage()).
                header("x-sql-state", exception.getSQLState()).
                header("x-error-code", exception.getErrorCode()).build();
    }
}
