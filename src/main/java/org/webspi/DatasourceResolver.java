/* 
 * Copyright (C) 2015-2016 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.webspi;

import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import javax.sql.DataSource;

/**
 *
 * @author Tim Fathers (tim@fathers.me.uk)
 * 
 * Datasource Resolver
 * Responsible to finding the correct datasource to be opened for the request.
 * The context is used to pass any relevant data from the request, such as
 * URL parameters etc.
 * The default implementation of this interface can be replaced with a custom
 * version and the beans.xml file changed accordingly.
 */
@ApplicationScoped // See http://stackoverflow.com/questions/7031885/how-to-choose-the-right-bean-scope
public interface DatasourceResolver {
    DataSource resolve(Map context) throws Exception;
}
