/* 
 * Copyright (C) 2015-2016 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.webspi.procedures.boundary;

import java.util.HashMap;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.webspi.procedures.entity.ClientPayload;
import org.webspi.procedures.entity.Procedure;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped // See http://stackoverflow.com/questions/7031885/how-to-choose-the-right-bean-scope
@Path("/sp")
public class ProcedureResource {

    @Inject
    ProcedureService ps;

    /**
     * Executes a stored procedure on the database. 
     * 
     * @param datasource
     * @param spName Name of the stored procedure to execute.
     * @param authToken Should be added to the HTTP header.
     * @param host
     * @param spParms The client payload. This must be
     * a JSON object containing the parameters to be passed to the stored 
     * procedure, for example:
     * <code>{"inputParms": {"PAGE_SIZE_": 9999, "PAGE_NUM_": 1}}</code> 
     * Where PAGE_SIZE_ and PAGE_NUM_ are the parameters to the procedure.
     * The Response is a JSON object.
     * @return
     * @throws Exception
     */
    @POST
    @Path("/{datasource}/{name}")
    public Response execute(
            @PathParam("datasource") String datasource,
            @PathParam("name") String spName,
            @HeaderParam("auth-token") String authToken,
            @HeaderParam("Host") String host,
            ClientPayload spParms
        ) throws Exception {

        Response.Status s;
        Map context = new HashMap();
        
        context.put("datasource", datasource);
        context.put("authToken", authToken);
        context.put("host", host);
        
        Procedure p = ps.execute(spName, spParms, context);

        /* Negative procedure return codes are interpreted as HTTP status codes. */
        int rc = p.getReturnValue();
        if (rc >= 0) {
            s = Response.Status.OK;
        } 
        else {
            s = Response.Status.fromStatusCode(-rc);
            if (s == null) {
                s = Response.Status.INTERNAL_SERVER_ERROR;
            }
        }

        return Response.status(s).entity(p).build();
    }

    /**
     *
     * @param datasource
     * @param name
     * @param host
     * @param authToken
     * @param host
     * @return
     * @throws Exception
     */
    @GET
    @Path("/{datasource}/{name}")
    public Procedure proc(
            @PathParam("datasource") String datasource,
            @PathParam("name") String name,
            @HeaderParam("auth-token") String authToken,
            @HeaderParam("Host") String host
        ) throws Exception {
        
        Map context = new HashMap();
        context.put("datasource", datasource);
        context.put("authToken", authToken);
        context.put("host", host);
        
        return ps.metadata(name, context);
    }
    
    /**
     *
     * @param datasource
     * @param name
     * @param authToken
     * @param host
     * @return
     * @throws Exception
     */
    @HEAD
    @Path("/{datasource}/{name}")
    public Response procHead(
            @PathParam("datasource") String datasource,
            @PathParam("name") String name,
            @HeaderParam("auth-token") String authToken,
            @HeaderParam("Host") String host
        ) throws Exception {
        
        Map context = new HashMap();
        context.put("datasource", datasource);
        context.put("authToken", authToken);
        context.put("host", host);
        
        ps.basicResolve(name, context);
        return Response.status(Response.Status.OK).build();
    }
    
}
