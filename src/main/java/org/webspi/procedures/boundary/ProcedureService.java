/* 
 * Copyright (C) 2015-2017 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.webspi.procedures.boundary;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ParameterMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.webspi.DatasourceConnectorService;
import org.webspi.StoredProcedureResolver;
import org.webspi.StoredProcedureResolver.ResolveType;
import org.webspi.procedures.entity.ClientPayload;
import org.webspi.procedures.entity.Procedure;
import org.webspi.procedures.entity.ProcedureParameter;

@Stateless
public class ProcedureService {
    private static final Logger LOGGER = Logger.getLogger(ProcedureService.class.getName());
    
    @Inject
    private DatasourceConnectorService ds;

    @Inject
    private StoredProcedureResolver resolver;

    /**
     * Does a full resolve to the stored procedure passed, returning the 
     * procedure names and schema as well as the parameter meta data. Used
     * by the GET method to allow clients to check on the validity and
     * permissions of a stored procedure and determine the parameter meta data.
     * 
     * @param spName
     * @param context
     * @return
     * @throws Exception
     */
    public Procedure metadata(String spName, 
                              Map context) throws Exception {
        Procedure proc;

        try (Connection con = ds.connect(context)) {
            proc = resolver.resolve(con, spName, context, ResolveType.FULL);
        }

        return proc;
    }

    /**
     * Does a basic resolve to the stored procedure passed, returning the 
     * procedure names and schema but not the parameter meta data. Used
     * by the HEAD method to allow clients to check on the validity and
     * permissions of a stored procedure.
     * 
     * @param spName
     * @param context
     * @return
     * @throws Exception
     */
    public Procedure basicResolve(String spName, 
                                  Map context) throws Exception {
        Procedure proc;

        try (Connection con = ds.connect(context)) {
            proc = resolver.resolve(con, spName, context, ResolveType.BASIC);
        }

        return proc;
    }

    /**
     * Executes a stored procedure on the given data source, using parameters
     * supplied in a context object.
     *                                                                                                                                     
     * @param spName
     * @param spParms
     * @param context
     * @return
     * @throws Exception
     */
    public Procedure execute(String spName, 
                             ClientPayload spParms, 
                             Map context) throws Exception {
        Procedure proc = null;

        LOGGER.log(Level.INFO, context.toString());

        try (Connection con = ds.connect(context)) {
            
            proc = resolver.resolve(con, spName, context, ResolveType.FULL);

            String statement = proc.asStatementString();
            
            try (CallableStatement cs = con.prepareCall(statement)) {
                // Setup the parameters before the stored proccedure call.
                setupParms(con, proc, cs, spParms);
                
                // Execute the stored procedure, check for warnings and any return value.
                boolean results = cs.execute();
                logExecuteWarnings(cs);
                
                proc.setReturnValue(cs.getInt(1)); // Return parameter
                
                // Add result sets to procedure object.
                while (results) {
                    ResultSet rs = cs.getResultSet();
                    if (rs != null) {
                        logResultSetWarnings(rs);
                        proc.addResultSet(rs);
                        rs.close();
                    }
                    results = cs.getMoreResults();
                }

                // Extract output parameter values and insert into the procedure object.
                extractReturnParms(proc, cs);
            }
        }

        return proc;
    }
    
    // Insert intput parameter values into the procedure object and register 
    // the output parameters.
    private void setupParms(Connection con, 
                            Procedure proc, 
                            CallableStatement cs, 
                            ClientPayload ctx) throws Exception {
        
        Map<String, ProcedureParameter> parms = proc.getParms();

        // Register the return value output parameter.
        cs.registerOutParameter(1, Types.INTEGER);  // Return parm
        
        for (Map.Entry<String, ProcedureParameter> entry : parms.entrySet()) {
            ProcedureParameter procParm = entry.getValue();
            ParameterMetaData md = cs.getParameterMetaData();
            
            int i = procParm.getIndex() + 1;

            if (isOutputParm(md.getParameterMode(i))) {
                cs.registerOutParameter(i, Types.VARCHAR);
            }

            String parmName = entry.getKey();
            if (isInputParm(md.getParameterMode(i))) {
                String typeName = md.getParameterTypeName(i);
                try {
                    Object value = ctx.getInputParameters().get(parmName);
                    if (isArray(value)) {
                        cs.setArray(i, createArrayParm(con, (ArrayList)value));
                    } else {
                        if (!"BLOB".equals(typeName) && !"BINARY LARGE OBJECT".equals(typeName)) {
                            cs.setObject(i, value);
                        } else {
                            cs.setObject(i, value == null ? null : Base64.getDecoder().decode(value.toString()));
                        }
                    }
                    // procParm.setValue(value); TODO CHECK THE IMPLICATIONS OF REMOVING THIS

                } catch (Exception ex) {
                    if ("TIMESTAMP".equals(typeName) ||
                        "DATE".equals(typeName) ||
                        "TIME".equals(typeName)) {
                        LOGGER.log(Level.INFO, "Potential date/time format connection configuration error, must be ISO.");
                    }
                    throw (new Exception("Failed to set parameter " + parmName));
                }
            }
        }
    }

    // Extract output parameter values and insert into the procedure object. 
    private void extractReturnParms(Procedure proc,
                                    CallableStatement cs) throws SQLException {
        Map<String, ProcedureParameter> parms = proc.getParms();
        ParameterMetaData p = cs.getParameterMetaData();

        for (HashMap.Entry<String, ProcedureParameter> parmObj : parms.entrySet()) {
            ProcedureParameter pp = parmObj.getValue();
            int i = pp.getIndex() + 1;
            if (isOutputParm(p.getParameterMode(i))) {
                if (pp.getIsArray()) {
                    pp.setValue(cs.getArray(i).getArray());
                } else {
                    String typeName = p.getParameterTypeName(i);
                    if (null != typeName) switch (typeName) {
                        case "TIMESTAMP": // Hack to get the timestamo into ISO 8601 format.
                            pp.setValue(cs.getTimestamp(i) == null ? null : cs.getDate(i).toString() + 'T' + cs.getTime(i).toString());
                            break;
                        case "DATE":
                        case "TIME":
                            pp.setValue(cs.getObject(i) == null ? null : cs.getObject(i).toString());
                            break;
                        case "BLOB":
                        case "BINARY LARGE OBJECT":
                            byte[] blobData = cs.getBytes(i); 
                            pp.setValue(blobData == null ? null : Base64.getEncoder().encodeToString(blobData));
                            break;
                        default:
                            pp.setValue(cs.getObject(i));
                            break;
                    }
                }
            }
        }
    }
    
    // Create an array parameter from an arrray object. 
    private java.sql.Array createArrayParm(Connection con, 
                                           ArrayList array) throws SQLException {
        List<String> sa = new ArrayList<>();
        array.stream().forEach((e) -> {
            sa.add(e.toString());
        });

        return con.createArrayOf("VARCHAR", sa.toArray(new String[0]));
    }
    
    private boolean isArray(Object value) {
        return value instanceof ArrayList;
    }
    
    private boolean isOutputParm(int pm) {
        return (pm == ParameterMetaData.parameterModeInOut || 
                pm == ParameterMetaData.parameterModeOut);
    }

    private boolean isInputParm(int pm) {
        return (pm == ParameterMetaData.parameterModeInOut || 
                pm == ParameterMetaData.parameterModeIn);
    }
    
    private void logExecuteWarnings(CallableStatement cs) throws SQLException {
        SQLWarning w = cs.getWarnings();
        while (w != null) {
            LOGGER.log(Level.WARNING, w.getMessage());
            w = w.getNextWarning();
        }
    }
    
    private void logResultSetWarnings(ResultSet rs) throws SQLException {
        SQLWarning w = rs.getWarnings();
        while (w != null) {
            LOGGER.log(Level.WARNING, w.getMessage());
            w = w.getNextWarning();
        }
    }
}
