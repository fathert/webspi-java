/* 
 * Copyright (C) 2015-2016 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.webspi.procedures.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(Include.NON_NULL) // Don't include null values in JSON 
public class ProcedureParameter {

    private final int index;
    private final String typeName;
    private final boolean isArray;
    private final int typeId;
    private final int length;
    private final int scale;
    private Object value;
    
    public int getIndex() {
        return index;
    }
  
    public ProcedureParameter(int index, String typeName, int typeId, int length, int scale) {
        this.index = index;
        this.typeName = typeName;
        this.isArray = typeName.contains("ARRAY");
        this.typeId = typeId;
        this.length = length;
        this.scale = scale;
        this.value = null;
    }
    
    public boolean getIsArray() {
        return this.isArray;
    }
    
    public String getTypeName() {
        return typeName;
    }
    
    public int getTypeId() {
        return typeId;
    }

    public int getLength() {
        return length;
    }

    public int getScale() {
        return scale;
    }

    public Object getValue() {
        return value;
    }    
    
    public void setValue(Object value) {
        this.value = value;
    }
}
