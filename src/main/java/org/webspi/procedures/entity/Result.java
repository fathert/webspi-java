/* 
 * Copyright (C) 2015-2017 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.webspi.procedures.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fathetx
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Result {

    @JsonInclude(JsonInclude.Include.NON_NULL) // Don't include null values in JSON 
    private List columnList;
    private final Map columnMetaData = new HashMap();
    private final List rows = new ArrayList();

    public Result(Boolean full) {
        if (full) {
            columnList = new ArrayList();
        }
    }
    
    public void addColumn(String name, Map Props) {
        if (columnList != null) {
            this.columnList.add(name);
        }
        this.columnMetaData.put(name, Props);
    }
    
    public void addRow(HashMap row) {
        rows.add(row);
    }
}
