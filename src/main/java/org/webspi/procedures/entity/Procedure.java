/* 
 * Copyright (C) 2015-2016 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.webspi.procedures.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_EMPTY) // Don't include null values in JSON 

public class Procedure {
    private String name;
    private String schema;
    private int returnValue;
    private Map<String, ProcedureParameter> parms;
    private final List resultSets = new ArrayList();
    
    public Procedure() {
        this.parms = new HashMap<>();
    }

    public Procedure(String schema, String name) {
        this();
        this.schema = schema;
        this.name = name;
    }

    public void setReturnValue(int returnValue) {
        this.returnValue = returnValue;
    }

    public int getReturnValue() {
        return returnValue;
    }
    
    public Map getParms() {
        return parms;
    }
    
    public void addParm(String name, ProcedureParameter parm) {
        parms.put(name, parm);
    }
    
    public void setParmValue(String parm, Object value) {
        parms.get(parm).setValue(value);
    }
    
    public String asStatementString() {
        String statement = "{? = call " + schema + "." + name + "(";
        if (this.parms.size() > 0) {
            statement += new String(new char[this.parms.size() - 1]).replace("\0", "?,") + "?";
        } 
        statement += ")}";

        return statement;
    }

    public void addResultSet(ResultSet rs) throws SQLException {
        if (rs==null) return;
        
        Result r = new Result(true);
        resultSets.add(r);
        
        ResultSetMetaData md;

        // Check result set null and don't add.
        md = rs.getMetaData();
        int cols = md.getColumnCount();

        for (int i = 1; i <= cols; i++) {
            HashMap col = new HashMap();
            /*col.put("name", md.getColumnName(i));*/
            col.put("type", md.getColumnType(i));
            col.put("typeName", md.getColumnTypeName(i));
            col.put("precision", md.getPrecision(i));
            col.put("scale", md.getScale(i));
            col.put("label", md.getColumnLabel(i));
            r.addColumn(md.getColumnName(i), col);
        }

        while (rs.next() != false) {
            HashMap row = new HashMap();
            r.addRow(row);

            for (int i = 1; i <= cols; i++) {
                String typeName = md.getColumnTypeName(i);
                if (null != typeName) switch (typeName) {
                    case "TIMESTAMP": // Hack to get the timestamo into ISO 8601 format.
                        row.put(md.getColumnName(i), rs.getTimestamp(i) == null ? null : rs.getDate(i).toString() + 'T' + rs.getTime(i).toString());
                        break;
                    case "DATE":
                    case "TIME":
                        row.put(md.getColumnName(i), rs.getObject(i) == null ? null : rs.getObject(i).toString());
                        break;
                    case "BINARY LARGE OBJECT":
                    case "BLOB":
                        byte[] blobData = rs.getBytes(i); 
                        row.put(md.getColumnName(i), blobData == null ? null : Base64.getEncoder().encodeToString(blobData));
                        break;
                    default:
                        row.put(md.getColumnName(i), rs.getObject(i));
                        break;
                }
            }
        }
    }

    public String getName() {
        return name;
    }

    public String getSchema() {
        return schema;
    }

    public List getResultSets() {
        return resultSets;
    }
}
