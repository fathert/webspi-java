/* 
 * Copyright (C) 2015-2017 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.webspi;

import java.sql.Connection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;

/*
 * Datasource Conn Resolver
 * Responsible for resolving to the correct stored procedure to call,
 * discovering its meta-data and creating a Procedure object representing
 * the procedure to be called.
 */

@Stateless
public class DefaultDatasourceConnectorService implements DatasourceConnectorService {
    private static final Logger LOGGER = Logger.getLogger(DefaultDatasourceConnectorService.class.getName());
    
    @Inject private DatasourceResolver dataSourceResolver;

    @Override
    public Connection connect(Map context) throws Exception {
        
        Connection con = dataSourceResolver.resolve(context).getConnection();
        try {
            this.postConnect(con, context);
        }
        catch(Exception ex) {
            con.close();
            throw(ex);
        }
        return con;
    }

    /**
     *
     * @param con
     * @param context
     * @throws Exception
     */
    @Override
    public void postConnect(Connection con, Map context) throws Exception {
       
        String authToken = null;
        if (context.containsKey("authToken")) {
            Object t = context.get("authToken");
            authToken = t == null ? null : t.toString();
        }
        
        // CRITICAL SERCUTIY NOTE!
        // It is vital that the "ClientUser" is not set to a null or empty 
        // string value as this can cause the server to leave the value
        // unchanged, therefore allowing an unauthenticated user to adopt the
        // identity of the previous user to use the database server job.
        if (authToken == null || authToken.trim().length() == 0) {
            authToken = "*NONE";
        }
        
        // For DB2/Win this requires the db2jcc4.jar driver in place, not db2jcc.jar. 
        con.setClientInfo("ClientUser", authToken);
        con.setClientInfo("ApplicationName", "WebSPi");
        LOGGER.log(Level.INFO, "Client info for DB connection: {0}", con.getClientInfo());
    }
}
