/* 
 * Copyright (C) 2015-2017 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.webspi;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import org.webspi.procedures.entity.Procedure;
import org.webspi.procedures.entity.ProcedureParameter;

/*
 * Stored Procedure Resolver
 * Responsible for resolving to the correct stored procedure to call,
 * discovering its meta-data and creating a Procedure object representing
 * the procedure to be called.
 */
@Stateless
public class DefaultStoredProcedureResolver implements StoredProcedureResolver {

    private static final Logger LOGGER = Logger.getLogger(DefaultStoredProcedureResolver.class.getName());

    @Override
    public Procedure resolve(Connection con, String spName, Map context, ResolveType resolveType) throws Exception {
        LOGGER.log(Level.INFO, "DefaultStoredProcedureResolver class used for procedure resolution");
        Procedure pr;

        try (CallableStatement cs = con.prepareCall("{? = call WSP_SP_RslvProc(?, ?, ?, ?)}")) {
            cs.registerOutParameter(1, Types.INTEGER);  // Return value
            cs.registerOutParameter(2, Types.VARCHAR);  // Returned schema
            cs.registerOutParameter(3, Types.VARCHAR);  // Returned specific name
            cs.setObject(4, spName);
            cs.setObject(5, resolveType == ResolveType.BASIC ? '1' : '0');
            cs.execute();
            pr = new Procedure(cs.getString(2), spName);

            if (resolveType != ResolveType.BASIC) {
                try (ResultSet rs = cs.getResultSet()) {
                    while (rs.next() != false) {
                        ProcedureParameter p = new ProcedureParameter(
                                rs.getInt("ORDINAL"),
                                rs.getString("TYPENAME"),
                                rs.getInt("TYPEID"),
                                rs.getInt("LENGTH"),
                                rs.getInt("SCALE")
                        );
                        pr.addParm(rs.getString("PARMNAME"), p);
                    }
                }
            }
        } // Return value

        return pr;
    }
}
