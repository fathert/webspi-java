/* 
 * Copyright (C) 2015-2016 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/*
  EXPERIMENTAL support for reading/writing database BLOBs
 */

package org.webspi.lobs.boundary;

import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.Map;
import javax.inject.Inject;
import org.webspi.DatasourceConnectorService;
import org.webspi.lobs.entity.DbInputStream;

public class DbBlobService {

    @Inject
    private DatasourceConnectorService ds;

    public void write(String resource, Map context, String mimeType, InputStream input) throws Exception {
        try {
            
            Connection con = ds.connect(context);
            
            CallableStatement s = con.prepareCall("{call SP_Content_CREATE(?, ?, ?)}");
            s.setString(1, resource);
            s.setString(2, mimeType);
            s.setBinaryStream(3, input);
            s.execute(); 
            
            con.close();

        } catch (Exception e) {
            throw (e);
        }
    }

    public DbInputStream getReaderStream(String resource, Map context) throws Exception {
        Connection con = ds.connect(context);

        return new DbInputStream(con, resource);
    }
}
