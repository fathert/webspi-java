/* 
 * Copyright (C) 2015-2016 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



/*
  EXPERIMENTAL support for reading/writing database BLOBs
  NOTE: Requires jtaManaged=false in the connection settings!
 */
package org.webspi.lobs.boundary;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.webspi.DefaultDatasourceConnectorService;
import org.webspi.lobs.entity.DbInputStream;

/**
 *
 * @author Tim Fathers (tim@fathers.me.uk)
 */
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.MULTIPART_FORM_DATA)
@RequestScoped // See http://stackoverflow.com/questions/7031885/how-to-choose-the-right-bean-scope
@Path("/resources")
public class DbBlobResource {

    /**
     * @param dataSource
     * @param authToken
     * @param body
     * @return
     * @throws SQLException
     * @throws java.io.IOException
     */
    @Inject
    DbBlobService rs;
    
    @PUT
    @Path("/{dataSource}")
    public Response upload(
            @PathParam("dataSource") String dataSource,
            @HeaderParam("auth-token") String authToken
    ) throws SQLException, IOException, Exception {
/*
        Attachment a = body.getAttachment("file");
        ContentDisposition cd = a.getContentDisposition();
        MediaType type = a.getContentType();
        InputStream inpStream = a.getDataHandler().getInputStream();
        String fileName = cd.getParameter("filename");

        rs.write(dataSource, authToken, fileName, type.toString(), inpStream);
        Logger.getLogger(DbBlobResource.class.getName()).log(Level.INFO, "Uploaded");

        return Response.status(Response.Status.CREATED).build();
*/
        return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
    }

    /**
     *
     * @param datasource
     * @param resource
     * @param authToken
     * @return
     * @throws SQLException
     * @throws Exception
     */
    @GET
    @Path("/{dataSource}/{resource}")
    public Response get(
            @PathParam("dataSource") String datasource,
            @PathParam("resource") String resource,
            @HeaderParam("auth-token") String authToken
    ) throws Exception {
        
        Map context = new HashMap();
        context.put("datasource", datasource);
        context.put("authToken", authToken);
        
        DbInputStream s = rs.getReaderStream(resource, context);
        
        System.out.println(s.getLength());
        Response.ResponseBuilder r;
        r = Response
            .ok(s)
            .header("content-disposition", "attachment; filename = " + resource)
            .type(s.getMIMEType())
            .lastModified(s.getModificationDate());
        
        return r.build();
    }

    /**
     *
     * @param datasource
     * @param resource
     * @param authToken
     * @return
     * @throws SQLException
     * @throws Exception
     */
    @GET
    @Path("/temp/{datasource}/{resource}")
    public Response getTempResource(
            @PathParam("datasource") String datasource,
            @PathParam("resource") String resource,
            @HeaderParam("auth-token") String authToken
    ) throws Exception {
        
        Map context = new HashMap();
        context.put("datasource", datasource);
        context.put("authToken", authToken);
        
        DbInputStream s = rs.getReaderStream(resource, context);
        
        System.out.println(s.getLength());
        Response.ResponseBuilder r;
        r = Response
            .ok(s)
            .header("content-disposition", "attachment; filename = " + s.getName())
            .type(s.getMIMEType())
            .lastModified(s.getModificationDate());
        
        return r.build();
    }    
}
