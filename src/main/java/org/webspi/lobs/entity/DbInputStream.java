/* 
 * Copyright (C) 2015-2016 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/*
  EXPERIMENTAL support for reading/writing database BLOBs
 */

package org.webspi.lobs.entity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public final class DbInputStream extends InputStream {

    private static final Logger LOGGER = Logger.getLogger(DbInputStream.class.getName());

    private String name;

    private String mimeType; 
    private long length;
    private Date modificationDate;

    private Connection conn;
    private CallableStatement c;
    private InputStream blobStream;
    private String resource;

    public DbInputStream(Connection conn, String resource) throws Exception {
        try {
            Blob data;
            String type = "!NOT FOUND!";

            this.conn = conn;
            this.resource = resource;

            this.c = conn.prepareCall("{call WTC_SP_Read(?, ?, ?, ?, ?, ?)}");
                
            /* Id */
            c.setString(1, resource);
            /* Name */
            c.registerOutParameter(2, Types.VARCHAR);
            /* MIME Type */
            c.registerOutParameter(3, Types.VARCHAR);
            /* Last modified timestamp */
            c.registerOutParameter(4, Types.TIMESTAMP);
            /* Data as VARBINARY addressed as a BLOB */
            c.registerOutParameter(5, Types.BLOB);
            /* Data as a BLOB */
            c.registerOutParameter(6, Types.BLOB);
            c.execute();

            this.name = c.getString(2);
            this.mimeType = c.getString(3);
            
            if (this.mimeType == null) {
                throw(new WebApplicationException("Resource " + resource + " not found.", Response.Status.NOT_FOUND));
            }
            
            this.modificationDate = c.getTimestamp(4);
            
            /*
             * Try to get the VARBINARY return value first, if it's NULL
             * then get the BLOB.
             */
            data = c.getBlob(5);
            if (data != null) {
                type = "VARBINARY";
            } else {
                data = c.getBlob(6);
                if (data != null) {
                    type = "BLOB";
                }
            }

            if (data == null) {
                throw(new WebApplicationException("Resource " + resource + " empty.", Response.Status.NO_CONTENT));
            }
            this.blobStream = new BufferedInputStream(data.getBinaryStream(), 32000);
            this.length = data.length();

            LOGGER.log(Level.INFO, "DBResourceInputStream opened ({0}): {1} {2}", new Object[]{type, this.toString(), new Timestamp(new java.util.Date().getTime())});
        } catch (Exception e) {
            this.close();
            throw (e);
        }
    }

    @Override
    public int read() throws IOException {
        return this.blobStream == null ? -1 : this.blobStream.read();
    }

    @Override
    public void close() throws IOException {
        if (this.blobStream != null) {
            try {
                this.blobStream.close();
                this.blobStream = null;
            }
            catch(IOException e) {
                LOGGER.log(Level.WARNING, "Failed to close non-null blob stream", e);
            }
        }
        
        if (this.c != null) {
            try {
                this.c.close();
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "Failed to close non-null callable statement", e);
            }
        }

        if (this.conn != null) {
            try {
                this.conn.close();
            }
            catch (SQLException e) {
                LOGGER.log(Level.WARNING, "Failed to close non-null callable connection", e);
            }
        }
            
        LOGGER.log(Level.INFO, "DBResourceInputStream closed: {0} {1}", new Object[]{this.toString(), new Timestamp(new java.util.Date().getTime())});
        super.close();
    }

    @Override
    public String toString() {
        return String.format("%s [%s] (%d bytes)", resource, this.mimeType,
                length);
    }

    public String getName() {
        return name;
    }
    
    public String getMIMEType() {
        return mimeType;
    }

    public long getLength() {
        return length;
    }

    public Date getModificationDate() {
        return modificationDate;
    }
}
