/* 
 * Copyright (C) 2015-2016 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.webspi;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionExceptionMapper implements ExceptionMapper<Exception> {
    private static final Logger LOGGER = Logger.getLogger(ExceptionExceptionMapper.class.getName());

    /**
     * SQLExceptionMapper
     * @param exception
     * @return
     * Maps an exception to an HTTP status code.
     */
    @Override
    public Response toResponse(Exception exception) {
        String message = exception.getMessage();
        
        LOGGER.log(Level.SEVERE, "Exception: " + message, exception);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .header("x-exception-message", message)
                .build();
    }
}
